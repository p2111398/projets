# Projets

Projets de Rocher Ludovic Polytech 2021 - 2022

**IsiWeb4Shop** :
Projet de web pour lequel j'ai appris à utiliser PHP en me connectant à une base de données via phpmyadmin, également l'utilisation de bootstrap pour le visuel du site. J'ai fait ce projet en très grande partie seul car j'ai trouvé ce sujet intéressant. J'ai passé environ 30h pour ce projet car je ne maitrisais pas dutout PHP et la connexion aux base de données lorsque j'ai commencé le projet.
Editeur de code : Visual Studio Code

**Jeu 2048** :
A partir d'un code Java qui renvoyait un jeu 2048 non fonctionnel, il fallait le faire fonctionner (jeu de base du 2048) puis ajouter des extensions comme la coloration des cases, pouvoir modifier la taille de la grille, afficher le score... Ce projet était assez difficile car mon binôme et moi maîtrisions mal le Java et il fallait coder en modulaire en respectant le modèle MVC. Je me suis concentré sur sur le fonctionnement du jeu en créeant les méthodes et mon binôme s'occupait de mettre en lien ces méthodes en respectant le modèle MVC. Nous avons passé environ 10h chacun sur ce projet.
Editeur de code : IntelliJ


**LaCave** : 
Le but était de construire un site web faisant appel à une base de données crée par nous même à l'aide du template Laravel. Pour cela nous avons utilisé une Machine Virtuelle Ubuntu pour respecter les demandes de l'exercice. Je me suis occupé en grande partie de la base de données et du CSS de notre site en utilisant un template, mon binôme s'est plus concentré sur les requêtes et les routes du projet. Nous avons passé environ 10h chacun sur ce projet.
Editeur de code : Visual Stuio Code

**Projet UDP** :
Projet en Java utilisant les DatagramSocket et DatagramPacket afin d'émeettre et recevoir des messages entre un Client et un Serveur. Cela nous a servi à approfondir notre codage en Java, globalement nous avons travaillé ensemble en binôme, ce qui nous a pris environ 10h de travail en groupe.
Editeur de code : IntelliJ

**Sudoku** :
Le but de ce projet était de coder un Sudoku en C#. L'exercice demandé était plus précisemment de générer une grille de sudoku respectant toutes les règels su Sudoku, pour ce projet cela m'a été difficle car nous n'avons eu que très peu de cours en C# ce qui fait que je partais de rien, c'est donc plus mon binôme qui s'est investit dans ce projet et m'a expliqué certaines choses pour que je puisse travailler de mon côté. Nous n'avons pas retravailler de C# après cela. Ce projet nous a pris environ 15h de travail.
Editeur de code : Visual Studio (2016)
